# Installed a stateful service (MongoDB) on Kubernetes using Helm 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Created a managed K8s cluster with Linode Kubernetes Engine

* Deployed replicated MongoDB service in LKE cluster using a Helm chart

* Configured data persistence for MongoDB with Linode’s cloud storage

* Deployed UI client Mongo Express for MongoDB

* Deployed and configured nginx ingress to access the UI application from browser

## Technologies Used 

* Kubernetes 

* Helm 

* MongoDB 

* Mongo Express 

* Linode LKE 

* Linux 

## Steps 

Step 1: Go to Linode and click on kubernetes 

[linode kubernetes](/images/01_creating_kubernetes_cluster_on_linode.png)

Step 2: Select two worker nodes and start cluster 

[Selecting Worker nodes](/images/02_selecting_two_worked_nodes.png)
[Creating nodes](/images/03_creating_nodes.png)
[Running Worker nodes](/images/04_running_worker_nodes.png)


Step 3: Download access file from Linode when the Kubernetes cluster is up and running 

[Download Linode Acees File](/images/05_download_linode_access_file_to_get_access_through_the_browser.png)

Step 4: Set Linode access file as an environment variable 

    export KUBECONFIG=~/Downloads/Test-kubeconfig.yaml 

[Linode Access file as Environment Variable](/images/06_set_linode_access_file_as_environmental_variable.png)

Step 5: Check if you are connected to server by checking for worker nodes in Linode

    kubectl get node

[Worker nodes](/images/07_check_if_we_are_connected_to_server_by_checking_worker_nodes_diplayed.png)

Step 6: Install helm 

    brew install helm 

[Install Helm](/images/08_install_helm.png)

Step 7: Check for Mongodb helmchart on browser 

[Mongodb helmchart on browser](/images/09_check_for_mongodb_helmchart.png)

Step 8: Add repo that contains the helmchart 

    helm repo add bitnami https://charts.bitnami.com/bitnami 

[Added repo](/images/10_adding_repository_that_contains_the_helmchart.png)

Step 9: Search for Mongodb in repo 

    helm search repo bitnami/mongo

[Searching for Mongodb](/images/11_searching_for_mongodb_in_repo.png)

Step 10: Check values you can change on the github 

[Checking Changeable Values](/images/12_check_values_you_can_change.png)

Step 11: Create a value file that contains values you want to change or add 

    touch test-mongodb.yaml 

[Creating value file](/images/13_create_a_values_file.png)
[Value File configuration](/images/13_value_file.png)

Step 12: Install mongodb chart in the cluster with values file option 

    helm mongodb --values test-mongodb.yaml bitnami/mongodb 

[Installing Mongodb Chart](/images/14_installing_mongodb_chart_in_cluster.png)
[Pods Running](/images/15_pods_running.png)
[Other Components](/images/16_other_components_that_got_created.png)
[Volumes](/images/19_for_each_pod_one_physical_storage_was_created.png)

Step 13: Create configuration file for mongo express deployment and service 

    touch mongo-express.yaml 

[Creating Mongo Express File](/images/20_creating_configuration_file_for_mongo_express.png)
[Mongo Express Configuration file](/images/21_mongo_express_configuration.png)

Step 13: Apply Mongo Express deployment and Service 

    kubectl apply -f mongo-express.yaml 

[Applying Mongo Express Configurations](/images/22_starting_mongo_express_deployment_and_service.png)

Step 14: Add Helm repo that contains ingress-nginx controller  

    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

[Adding Helm repo for ingress-nginx](/images/23_adding_nginx_controller_repo.png)

Step 15: Install ingress chart 

    helm install nginx-ingress-nginx/ingress-nginx --set controller.publishService.enabled=true 

[Installing ingress chart](/images/24_installing_the_ingress_chart.png)

Step 16: Check if ingress controller was deployed

    kubectl get pod 

[Ingress Controller pod](/images/25_check_if_ingress_controller_was_deployed.png)

Step 17: Create ingress file to set ingress rule 

    touch ingress.yaml 

[Creating Ingress file](/imsges/26_creating_ingress_file.png)

Step 18: Take host name from loadbalancer because it points at the ip address of the loadbalancer 

[Loadbalancer host name](/images/27_took_host_name_of_loadbalancer_because_it_points_at_the_ip_address_to_put_in_ingress_rule.png)

Step 19: Attach ingress rules in ingress file 

[ingress rules](/images/28_ingress_rules_in_ingress_file.png)

Step 20: Apply ingress rulles 

    kubectl apply -f ingress.yaml 

[Applying Ingress](/images/29_creating_ingress_rule.png)

Step 21: Confirm if ingress has been created 

    kubectl get ingress  

[Confirm Created ingress](/images/30_confirm_if_ingress_has_been_created.png)

Step 22: Confirm if mongo express is accessible through browser 

[Mongo Express on Browser](/images/31_confirming_if_mongo_express_is_accessible_through_browser_with_host_name.png)

Step 23: Test data persistency by removing all replicas 

    kubectl scale --replicas=0 statefulset/mongodb 

[Removing all replicas](/images/32_test_data_romving_all_replicas.png)

Step 24: Attach replicas back 

    kubectl scale --replicas=3 statefulset/mongodb

[Adding replicas back](/images/33_reattach_replicas.png)

Step 25: Check if volumes got reattached on Linode 

[Check Volumes getting reattached](/images/34_you_can_see_volumes_are_reattaching_themselves_to_replicas.png)
[Created Database still available](/images/35_created_database_is_still_available.png) 

## Installation

Run $ brew install minikube 

## Usage 

Run $ kubectl apply -f Mongo-express.yaml 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/k8-helm.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/k8-helm

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.